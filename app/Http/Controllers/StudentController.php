<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return view('form')->with('students',$students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
         //'blood'  =>'required',
         //'gender' => 'required',
         'photo'  => 'nullable|image',
         'cv'     => 'nullable|file',

        ]);
        
        if($request->hasFile('photo'))
        {
            $fileObject      = $request->file('photo');
            $extension       = $fileObject->getClientOriginalExtension();
            $mimeType        = $fileObject->getClientMimeType();
            $fileName        = $fileObject->getClientOriginalName();
            $size            = $fileObject->getClientSize();
            $photoPath       = $fileObject->storeAs('PhotoUploads',$request->input('name').'_photo.'.$extension);
            $photo           = $request->input('name').'_photo.'.$extension; 
        }
        if($request->hasFile('cv'))
        {
            $fileObject   = $request->file('cv');
            $extension    = $fileObject->getClientOriginalExtension();
            $mimeType     = $fileObject->getClientMimeType();
            $fileName     = $fileObject->getClientOriginalName();
            $size         = $fileObject->getClientSize();
            $cvPath       = $fileObject->storeAs('CVuploads',$request->input('name').'_CV.'.$extension);
            $cv           = $request->input('name').'_CV.'.$extension;   
        }
      
        $s = new Student();
        $s->stage    = $request->input('stage');
        $s->name     = $request->input('name');
        $s->photo    = $photo;
        $s->phone    = $request->input('phone');
        $s->address  = $request->input('address');
        //$s->blood   = $request->input('blood');
        $s->gender   = $request->input('gender');
        $s->cv       = $cv;
        $s->save();
        
        return redirect(route('form.index')); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('form')->with('student',$student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
         //'blood'  =>'required',
         //'gender' => 'required',
         'photo'  => 'nullable|image',
         'cv'     => 'nullable|file',

        ]);
        
        if($request->hasFile('photo'))
        {
            $fileObject      = $request->file('photo');
            $extension       = $fileObject->getClientOriginalExtension();
            $mimeType        = $fileObject->getClientMimeType();
            $fileName        = $fileObject->getClientOriginalName();
            $size            = $fileObject->getClientSize();
            $photoPath       = $fileObject->storeAs('PhotoUploads',$request->input('name').'_photo.'.$extension);
            $photo           = $request->input('name').'_photo.'.$extension; 
        }
        if($request->hasFile('cv'))
        {
            $fileObject   = $request->file('cv');
            $extension    = $fileObject->getClientOriginalExtension();
            $mimeType     = $fileObject->getClientMimeType();
            $fileName     = $fileObject->getClientOriginalName();
            $size         = $fileObject->getClientSize();
            $cvPath       = $fileObject->storeAs('CVuploads',$request->input('name').'_CV.'.$extension);
            $cv           = $request->input('name').'_CV.'.$extension;   
        }
      
        $s =  Student::find($id);
        $s->stage       = $request->input('stage');
        $s->name        = $request->input('name');
        if($request->hasFile('photo'))
        {
           $s->photo    = $photo;
        }
        $s->phone       = $request->input('phone');
        $s->address     = $request->input('address');
        //$s->blood     = $request->input('blood');
        $s->gender      = $request->input('gender');
        if($request->hasFile('cv')) 
        {
           $s->cv       = $cv;
        }
        $s->save();
        
        return redirect(route('form.index')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->destroy($id);
        return redirect(route('form.index'));
    }
}
