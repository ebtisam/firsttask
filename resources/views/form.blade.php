<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>framework</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/style.css')}}">

	<style>
	.frame {
		padding: 30px;
		background-color: #fbfbfb;
	}
	.frame .box{
		padding: 30px;
		box-shadow: 0 0 3px rgba(206, 206, 206, 0.36);
		background-color: #fff;
	}
	.frame .left{
		float: left;
		margin-left: 20px;
		margin-top: 5px;
	}

	.frame textarea{
		width: 100%;
	}
	.form-group{
		margin-bottom: 10px;
	}
	.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
		vertical-align: middle;
		text-align:center;
	}
	thead{
		color: #fff;
		background-color: #1b254a;
	}
	.color1{
		background-color: #0d5398;
		color: #fff;
	}
	.radio,.checkbox{
		display: inline-block;
		padding: 0 30px 0 0;
	}
	.heig{
		height: 40px;
	}

</style>

</head>
<body>

	<section class="frame">
		<div class="container">	
	    @if (isset($student))	
	    <form action="{{route('form.update',$student->id)}}" method="post" enctype="multipart/form-data">
	    <input type="hidden" name="_method" value="PUT"/>	
	    @else	
        <form action="{{route('form.store')}}" method="post" enctype="multipart/form-data">
        @endif	
        	{{csrf_field()}}
        	<div class="row">
			<div class="col-sm-6 box">
			@if($errors->any())	
			    <ul>
				      @foreach($errors->all() as $error)

				      <div class="alert alert-primary" role="alert">
						 {{$error}}
					  </div>
				      @endforeach
			     </ul>
			    @endif
				<div class="form-group row">
					<div class="col-sm-3 color1">
						<label class="left">Stage</label>
					</div>
					<div class="col-sm-9">
						<select name="stage" class="form-control" required>
						<option ></option>
						<option value="1st" @if (isset($student) && $student->stage == '1st') selected
						@elseif (old('stage') == '1st') selected @endif>first</option>
						<option value="2nd" @if (isset($student) && $student->stage == '2nd') selected
							@elseif (old('stage') == '2nd') selected @endif>second</option>
						<option value="3rd" @if (isset($student) && $student->stage == '3rd') selected
							@elseif (old('stage') == '3rd') selected @endif>third</option>
						</select>
					</div>
				</div>
				<hr>
				<div class="clearfix"></div> 
				<div class="form-group row">
					<div class="col-sm-3 color1">
						<label class="left">Student</label>

					</div>
					<div class="col-sm-9">
						<input type="text" name="name" value="@if(isset($student)) {{$student->name}}@else{{old('name')}}@endif" required placeholder="EX: Ebtsam Nasr" class="form-control">
					</div>
				</div> 
				<hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1">
						<label class="left">Photo</label>
					</div>

					<div class="col-sm-9">
						<input type="file" name="photo" value="@if(isset($student)) {{$student->photo}}@else{{old('photo')}}@endif" id="imageupload" @if(isset($students))required @endif>
						<br>
					</div>
				</div>
				<hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1">
						<label class="left">Phone</label>
					</div>
					<div class="col-sm-9">
						<input type="number" name="phone" required placeholder="EX: 0102564421" value="@if(isset($student)){{$student->phone}}@else{{old('phone')}}@endif" class="form-control">
					</div>
				</div>
				<hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1 heig">
						<label class="left">Address</label>
					</div>
					<div class="col-sm-9">
						<textarea class="form-control" name="address" required >@if(isset($student)) {{$student->address}}@else{{old('address')}}@endif</textarea>
					</div>
				</div>
				<!--hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1 heig">
						<label class="left ">Blood</label>
					</div>
					<div class="col-sm-9">
						<div class="checkbox">
							<label>
						     <input type="checkbox" name="Blood" value="A"> A
						    </label>
						</div> 
						<div class="checkbox">
							<label>
						     <input type="checkbox" name="blood" value="O"> O
						    </label>
						</div> 
						<div class="checkbox">
							<label>
						     <input type="checkbox" name="blood" value="B"> B
						    </label>

						</div>    
	
					</div>
				</div-->
				<hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1 heig">
						<label class="left">Gender</label>
					</div>
					<div class="col-sm-9">
						<div class="radio">
							<input type="radio" name="gender" value="Male" @if(isset($student) && $student->gender == "Male") checked @elseif(old('gender') == "Male") checked  @endif required>
							<label >Male</label>
						</div>
						<div class="radio">
							<input type="radio" name="gender" value="Female"@if(isset($student) && $student->gender == "Female") checked @elseif(old('gender') == "Female") checked  @endif>
							<label >Female</label>
						</div>
					</div>
				</div>
				<hr>
				<div class="clearfix"></div>
				<div class="form-group row">
					<div class="col-sm-3 color1">
						<label  class="left" for="fileupload">CV</label>
					</div>
					<div class="col-sm-9">
						<input type="file" name="cv" value="@if(isset($student)){{$student->cv}} @else{{old('cv')}}@endif" id="fileupload" @if(isset($students))required @endif>
						@if(isset($student))
						    <a href="/storage/cvUploads/{{$student->cv}}" download="{{$student->cv}}">Download</a>
						@endif
					</div>
				</div>
				<hr>
				<div class="clearfix"></div>
				<div class="form-group text-center">
					<button class="btn btn-md btn-danger " name="add" type="submit">Add</button>
				</div>
			</div>
			<div class="col-sm-6 box">
			    
				<img id="blah" @if(isset($student)) src="/storage/photoUploads/{{$student->photo}}" @endif alt="your photo"  width="100%" height="400" />
                
			</div>
			</div> 
		 </form>
		</div>
	</section>
	<section class="output">
		<div class="container">
			<table class="table table-bordered table-striped">
				<thead>
					<th>#</th>
					<th>Stage</th>
					<th>Student</th>
					<th>Phone</th>
					<th>Address</th>
					<!--th>Blood</th-->
					<th>Gender</th>
					<th>Action</th>
				</thead>
				<tbody>
					@if(isset($students) && count($students) > 0)
					   @foreach($students as $student)
					      <tr>
							<td>{{$student->id}}</td>
							<td>{{$student->stage}}</td>
							<td>{{$student->name}}</td>
							<td>{{$student->phone}}</td>
							<td>{{$student->address}}</td>
							<!--td>{{$student->blood}}</td-->
							<td>{{$student->gender}}</td>
							<td>
								<div class="btn-group">
                                  
									<a class="btn btn-xs btn-info" href="{{route('form.edit',$student->id)}}" >
										<i class="ace-icon fa fa-pencil bigger-120"></i>
									</a>
                                   
	                                 <form action="{{route('form.destroy',$student->id)}}"  method="post" >
	                                 	{{csrf_field()}}
	                                 	{{method_field('DELETE')}}
										<button class="btn btn-xs btn-danger" id="delete" onClick="return confirm('Are you sure to delete that record?')" href="">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</button>
								    </form>
								</div>
							</td>
					    </tr>
					   @endforeach
					@endif
					  
					</tbody>
						
    <script type="text/javascript" src="{{asset('js/jquery-1.10.1.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script>
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#blah').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#imageupload").change(function(){
			readURL(this);
		});
	</script>
</body>
</html>
